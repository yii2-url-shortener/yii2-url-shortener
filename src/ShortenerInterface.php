<?php

namespace mikk150\urlshortener;

interface ShortenerInterface
{
    /**
     * Sortens given URL
     *
     * If url cannot be shortened, false is returned
     *
     * @param $url
     * @return string|bool
     */
    public function shorten($url);
}
